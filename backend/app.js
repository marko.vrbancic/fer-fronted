const { MongoClient } = require('mongodb');
const cors = require('cors');
const dotenv = require('dotenv');
const express = require('express');
const http = require('http');
const { Server } = require('socket.io');
const { v4: uuidv4 } = require('uuid');
const path = require('path');

dotenv.config();

class App
{
    constructor()
    {
        const me = this;
        me.mdb = null;
        me.adminSockets = {};
        me.clientSocket = {};
    }

    async init()
    {
        const me = this;
        await me.connectToMongo();

        const app = express();

        const server = http.createServer(app);

        app.use(cors());
        app.use('/chat', express.static(path.join(__dirname, 'chat')));

        app.get('/history/:id', async (req, res) =>
        {
            const id = String(req.params.id);
            const query = { _id: id };
            let data = {};

            const dataObject = await me.mdb.collection('chats').findOne(query);
            if (dataObject != null)
            {
                data = dataObject;
            }

            res.status(200);
            res.send(data);
        });
        app.get('/history', async (req, res) =>
        {
            let data = {};

            const dataObject = await me.mdb.collection('chats')
                .find().sort({ _id: 1 })
                .limit(50)
                .toArray();
            if (dataObject != null)
            {
                data = dataObject;
            }

            res.status(200);
            res.send(data);
        });

        me.io = new Server(server, {
            cors: {
                origin: ['http://localhost:8080', 'http://localhost:8080/admin'],
                methods: ['GET', 'POST'],
            },
        });

        // await me.createCollec();
        // await this.insertTestData();

        me.wsClients = [];
        me.io.on('connection', (serverSocket) =>
        {
            console.log('Made socket connection');

            // checking if user already exists?? on connect?

            serverSocket.on('admin', async () =>
            {
                try
                {
                    console.log('admin connected');

                    if (me.adminSockets[serverSocket.id] === undefined)
                    {
                        me.adminSockets[serverSocket.id] = serverSocket;
                        console.log(`admins socket ${serverSocket.id}`);
                    }
                }
                catch (err)
                {
                    console.error(err);
                }
            });


            serverSocket.on('newClient', async (data) =>
            {
                const useruuid = data.sessionId;
                const chatTimeStamp = Date.now();
                const startChat = {
                    insertTimeStamp: chatTimeStamp,
                    lastUpdateTimestamp: chatTimeStamp,
                    nameLastName: data.nameLastName,
                    email: data.email,
                    messages: [],
                    chatId: useruuid
                };

                await me.mdb.collection('chats').updateOne(
                    {
                        _id: useruuid,
                    },
                    {
                        $set: startChat,
                    },
                    {
                        upsert: true,
                    },
                );

                me.clientSocket[useruuid] = serverSocket;

                // serverSocket.emit("adminMessage", useruuid);
            });

            serverSocket.on('disconnect', () =>
            {
                console.warn('Something went wrong.');
            });

            // admin send response to user, emit to topic where user is listening
            serverSocket.on('adminResponse', async (data) =>
            {
                const { useruuid } = data;
                await this.pushMsgToMongo(useruuid, data.message, data.lastUpdate);

                console.log('Emit to client');

                me.clientSocket[useruuid].emit('clientMessage', data);
            });

            // user sends message to admin, emit to admin and save into mongo
            serverSocket.on('userResponse', async (data) =>
            {
                const useruuid = data.sessionId;
                await this.pushMsgToMongo(useruuid, data.message, data.lastUpdate);

                if (me.clientSocket[useruuid] == null)
                {
                    me.clientSocket[useruuid] = serverSocket;
                }

                console.log('Emit msg');
                for (const socketId in me.adminSockets)
                    {
                        if (socketId !== undefined)
                        {
                            me.adminSockets[socketId].emit('adminMessage', data);
                        }
                    }

            });
        });

        server.listen(process.env.PORT, () => {  console.log(`listening on ${process.env.PORT}`);});
    }

    async pushMsgToMongo(useruuid, message, lastUpdateTimestamp)
    {
        const me = this;
        const query = { _id: useruuid };
        let msgs = [];

        const dataObject = await me.mdb.collection('chats').findOne(query);
        if (dataObject != null)
        {
            msgs = dataObject.messages;
        }
        else
        {
            msgs = [];
        }

        msgs.push(message);

        const toUpdate = {
            lastUpdateTimestamp,
            messages: msgs,
        };

        await me.mdb.collection('chats').updateOne(
            {
                _id: useruuid,
            },
            {
                $set: toUpdate,
            },
        );
    }

    async createCollec()
    {
        const me = this;
        me.mdb.createCollection('chats', (err) =>
        {
            if (err)
            {
                throw err;
            }
            console.log('Collection created!');
        });
    }

    async insertTestData()
    {
        const me = this;
        const test = {
            _id: uuidv4(),
            insertTimeStamp: 5599559959,
            lastUpdateTimestamp: 2566997878,
            nameLastName: 'John Doe',
            email: 'john.doe@leapbit.com',
            messages: [
                {
                    msg: 'Dobar dan',
                    admin: true,
                    timestamp: 123455665989,
                },
                {
                    msg: 'Ne radi mi internet',
                    admin: false,
                    timestamp: 123455665989,
                },
            ],
        };
        me.mdb.collection('chats').insertOne(test, (err) =>
        {
            if (err)
            {
                throw err;
            }
            console.log('Data inserted!');
        });
    }

    async connectToMongo()
    {
        const me = this;
        const client = new MongoClient(process.env.MONGOURL);
        await client.connect();
        me.mdb = client.db('test');
    }
}

const a = new App();
a.init();
