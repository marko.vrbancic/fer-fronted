import { createWebHistory, createRouter } from "vue-router";
import ChatWin from "@/views/user/ChatWin.vue";
import AdminWin from "@/views/admin/AdminWin.vue";

const routes = [
  {
    path: "/",
    name: "ChatWin",
    component: ChatWin,
  },
  {
    path: "/admin",
    name: "AdminWin",
    component: AdminWin,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;