import { createApp } from 'vue';
import VueSocketIOExt from 'vue-socket.io-extended';
import { io } from 'socket.io-client';
import withUUID from 'vue-uuid';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './App.vue';
import router from './router';
import dayjs from 'dayjs';
import '../assets/css/bootstrap.min.css';
import '../assets/css/custom.css';
import '../assets/css/responsive.css';

const socket = io('http://localhost:8000');

const app = withUUID(
    createApp(App)
        .use(router)
        .use(VueSocketIOExt, socket)
        .use(VueAxios, axios),
);

// app.config.globalProperties.$socket = socket;
app.config.globalProperties.$dayjs = dayjs;

app.mount('#app');
